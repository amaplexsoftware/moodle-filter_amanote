define(['jquery', 'core/modal_factory'], function ($, modalFactory) {
    var Main = (function () {
        function Main() {
        }
        Main.prototype.init = function (rawUserParams) {
            var pluginParams = window['amanote_params'];
            var moodleUserParams = Main.parseParams(rawUserParams);
            if (!pluginParams || !moodleUserParams) {
                return;
            }
            MoodleService.init(pluginParams, moodleUserParams, modalFactory);
            var courseModuleFilter = new CourseModuleFilter();
            courseModuleFilter.addButtonToCourseModules();
        };
        Main.parseParams = function (rawParams) {
            try {
                return JSON.parse(rawParams);
            }
            catch (error) {
                console.error(error);
                return null;
            }
        };
        return Main;
    }());
    var MoodleService = (function () {
        function MoodleService(pluginParams, moodleUserParams, modalFactory) {
            this.pluginParams = pluginParams;
            this.moodleUserParams = moodleUserParams;
            this.modalFactory = modalFactory;
            if (MoodleService.instance) {
                throw new Error("Error - Use MoodleService.getInstance()");
            }
            try {
                var userOpeningMode = localStorage.getItem(StorageKeysEnum.OpeningMode);
                if (userOpeningMode !== null) {
                    this.pluginParams.plugin.openingMode = userOpeningMode;
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        MoodleService.prototype.getPluginParams = function () {
            return this.pluginParams;
        };
        MoodleService.prototype.getUserParams = function () {
            return this.moodleUserParams;
        };
        MoodleService.prototype.getModalFactory = function () {
            return this.modalFactory;
        };
        MoodleService.prototype.getAnnotatableByCmId = function (cmid) {
            return (this.pluginParams.annotatables || [])
                .filter(function (cm) { return cm.cmid == cmid; }).pop();
        };
        MoodleService.prototype.getAnnotatableById = function (id) {
            return (this.pluginParams.annotatables || [])
                .filter(function (a) { return a.id == id; }).pop();
        };
        MoodleService.prototype.getAnnotatableByContentPath = function (path) {
            var annotatable = this.pluginParams.annotatables || [];
            for (var i = 0; i < annotatable.length; i++) {
                if (annotatable[i].url === path) {
                    return annotatable[i];
                }
                else if (annotatable[i].internal) {
                    var path1 = annotatable[i].url;
                    var path2 = path.split('pluginfile.php')[1]
                        .split('?')[0]
                        .replace('intro/0', 'intro')
                        .replace('content/0/', 'content/1/');
                    path1 = decodeURIComponent(path1 || '');
                    path2 = decodeURIComponent(path2 || '');
                    if (path1 && path2 && path1 === path2) {
                        return annotatable[i];
                    }
                }
            }
            return null;
        };
        MoodleService.prototype.getSavedNoteFilenameForAnnotatable = function (annotatable) {
            var savedNotes = this.pluginParams.savedNotes || {};
            if (savedNotes[annotatable.id + '.ama']) {
                return savedNotes[annotatable.id + '.ama'].filename;
            }
            else if (annotatable.legacyid && savedNotes[annotatable.legacyid + '.ama']) {
                return savedNotes[annotatable.legacyid + '.ama'].filename;
            }
            return null;
        };
        MoodleService.prototype.getLogoForAnnotatable = function (annotatable) {
            if (this.getSavedNoteFilenameForAnnotatable(annotatable)) {
                return this.pluginParams.plugin.annotatedLogo;
            }
            return this.pluginParams.plugin.logo;
        };
        MoodleService.prototype.generateAmanoteURL = function (annotatable, route) {
            if (route === void 0) { route = 'note-taking'; }
            if (!annotatable) {
                return '';
            }
            if (route === 'note-taking' && this.pluginParams.plugin.target != OpeningTargetEnum.Amanote) {
                return "".concat(this.pluginParams.siteURL, "/filter/amanote/annotate.php?annotatableId=").concat(annotatable.id);
            }
            var filePath = annotatable.url;
            if (annotatable.internal && filePath.indexOf('pluginfile.php') >= 0) {
                filePath = filePath.split('pluginfile.php')[1].replace('content/0/', 'content/1/');
            }
            else {
                filePath = encodeURIComponent(filePath);
            }
            var noteFilename = this.getSavedNoteFilenameForAnnotatable(annotatable) || "".concat(annotatable.id, ".ama");
            var amaPath = this.pluginParams.privateFilePath + noteFilename;
            var protocol = 'https';
            if (this.pluginParams.siteURL.indexOf('https') < 0) {
                protocol = 'http';
            }
            if (route === 'note-taking' && annotatable.kind === ContentKindEnum.Video) {
                route = "/note-taking/moodle/video/".concat(annotatable.id);
            }
            else {
                route = "/moodle/".concat(route);
            }
            return protocol + '://app.amanote.com/' + this.pluginParams.language + route + '?' +
                'siteURL=' + this.pluginParams.siteURL + '&' +
                'accessToken=' + this.moodleUserParams.token.value + '&' +
                'tokenExpDate=' + this.moodleUserParams.token.expiration + '&' +
                'userId=' + this.moodleUserParams.id + '&' +
                'filePath=' + filePath + '&' +
                'mimeType=' + annotatable.mimetype + '&' +
                'amaPath=' + amaPath + '&' +
                'resourceId=' + annotatable.id + '&' +
                'legacyResourceId=' + (annotatable.legacyid || annotatable.id) + '&' +
                'saveInProvider=' + (this.pluginParams.plugin.saveInProvider ? '1' : '0') + '&' +
                'providerVersion=' + this.pluginParams.moodle.version + '&' +
                'pluginVersion=' + this.pluginParams.plugin.version + '&' +
                'key=' + this.pluginParams.plugin.key + '&' +
                'worksheet=' + (this.pluginParams.plugin.worksheet ? '1' : '0') + '&' +
                'anonymous=' + (this.pluginParams.plugin.anonymous ? '1' : '0');
        };
        MoodleService.init = function (pluginParams, moodleUserParams, modalFactory) {
            MoodleService.instance = new MoodleService(pluginParams, moodleUserParams, modalFactory);
        };
        MoodleService.getInstance = function () {
            return MoodleService.instance;
        };
        return MoodleService;
    }());
    var CourseModuleFilter = (function () {
        function CourseModuleFilter() {
            this.menuModal = new MenuModal();
            this.moodleService = MoodleService.getInstance();
            this.params = this.moodleService.getPluginParams();
            this.userParams = this.moodleService.getUserParams();
        }
        CourseModuleFilter.prototype.addButtonToCourseModules = function () {
            var _this = this;
            this.addAmanoteButtons();
            if (this.observer) {
                this.observer.disconnect();
            }
            this.observer = new MutationObserver(function (mutationsList) {
                if (_this.doesMutationsContainAnActivity(mutationsList)) {
                    _this.addAmanoteButtons();
                }
            });
            var targetNode = document.getElementById('page-content');
            this.observer.observe(targetNode, { childList: true, subtree: true });
        };
        CourseModuleFilter.prototype.doesMutationsContainAnActivity = function (mutationsList) {
            for (var i = 0; i < mutationsList.length; i++) {
                var mutation = mutationsList[i];
                if (mutation.type !== 'childList') {
                    continue;
                }
                for (var j = 0; j < mutation.addedNodes.length; j++) {
                    var addedNode = mutation.addedNodes[j];
                    if ($(addedNode).find('.activityinstance, .activity-instance').length > 0) {
                        return true;
                    }
                }
            }
            return false;
        };
        CourseModuleFilter.prototype.addAmanoteButtons = function () {
            var _this = this;
            this.forEachNewInstances(['modtype_resource', 'modtype_url'], function (element) {
                var annotatable = _this.getAnnotatableFromElement(element);
                if (!annotatable) {
                    return;
                }
                var activityLink = $(element).find('.activitytitle').find('a').first();
                if (activityLink.length === 0) {
                    activityLink = $(element).find('a').first();
                }
                if (_this.openWithButton()) {
                    var button = _this.generateAmanoteButton(annotatable);
                    activityLink.css('display', 'inline-block');
                    activityLink.removeClass('stretched-link');
                    if ($(element).find('.activity-instance').length > 0) {
                        $(element).find('.activity-instance').find('.activityname').children().first().after(button);
                    }
                    else if ($(element).find('.activity-basis').length > 0) {
                        $(element).find('.activity-basis').first().children().children().first().after(button);
                    }
                    else {
                        $(element).find('.activityinstance').first().children().first().after(button);
                    }
                }
                else {
                    _this.replaceLink(activityLink, annotatable);
                    var iconLink = $(element).find('a.activity-icon').first();
                    if (iconLink.length > 0) {
                        _this.replaceLink(iconLink, annotatable);
                    }
                }
                _this.addOnDeleteWarning($(element));
            });
            this.forEachNewInstances(['fp-filename-icon'], function (element) {
                var fileLink = $(element).find('a').first();
                if (fileLink.length !== 1) {
                    return;
                }
                var filePath = fileLink.attr('href');
                var annotatable = _this.moodleService.getAnnotatableByContentPath(filePath);
                if (!annotatable) {
                    return;
                }
                if (_this.openWithButton()) {
                    var button = _this.generateAmanoteButton(annotatable);
                    fileLink.css('display', 'inline-block');
                    fileLink.after(button);
                }
                else {
                    _this.replaceLink(fileLink, annotatable);
                }
            });
            this.forEachNewInstances(['modtype_folder'], function (element) {
                _this.addOnDeleteWarning($(element));
            });
            this.forEachNewInstances(['modtype_label'], function (element) {
                var annotatable = _this.getAnnotatableFromElement(element);
                if (!annotatable) {
                    return;
                }
                if (_this.openWithButton()) {
                    var button = _this.generateAmanoteButton(annotatable);
                    $(element).find('.mediaplugin').first().children().children().first().after(button);
                }
                else {
                    _this.replaceLink($(element).find('a').first(), annotatable);
                }
                _this.addOnDeleteWarning($(element));
            });
            setTimeout(function () {
                $(".".concat(CourseModuleFilter.amanoteButtonClass)).on('click', function (event) {
                    event.preventDefault();
                    var annotatableId = $(event.currentTarget).attr(CourseModuleFilter.annotatableIDAttribute);
                    var annotatable = _this.moodleService.getAnnotatableById(annotatableId);
                    if (_this.params.plugin.openingMode === OpeningModeEnum.FileClick) {
                        annotatable.openInMoodleURL = event.currentTarget.href;
                    }
                    if ((_this.params.plugin.openingMode !== OpeningModeEnum.FileClick || _this.params.plugin.preventDownload) && !_this.userParams.isTeacher) {
                        window.open(_this.moodleService.generateAmanoteURL(annotatable, 'note-taking'), 'blank');
                        return;
                    }
                    _this.menuModal.open(annotatable);
                });
            }, 500);
        };
        CourseModuleFilter.prototype.getAnnotatableFromElement = function (element) {
            var elementId = $(element).attr('id');
            if (!elementId || elementId.indexOf('module-') < 0) {
                return;
            }
            var courseModuleId = parseInt(elementId.replace('module-', ''), 10);
            return this.moodleService.getAnnotatableByCmId(courseModuleId);
        };
        CourseModuleFilter.prototype.openWithButton = function () {
            return this.params.plugin.openingMode !== OpeningModeEnum.FileClick;
        };
        CourseModuleFilter.prototype.forEachNewInstances = function (classNames, action) {
            $('.' + classNames.join(', .')).each(function (index, element) {
                if ($(element).find('.' + CourseModuleFilter.amanoteButtonClass).length > 0) {
                    return;
                }
                action(element);
            });
        };
        CourseModuleFilter.prototype.generateAmanoteButton = function (annotatable) {
            var _a;
            var moodleService = MoodleService.getInstance();
            var logo = moodleService.getLogoForAnnotatable(annotatable);
            var widthByMode = (_a = {},
                _a[OpeningModeEnum.FileClick] = 90,
                _a[OpeningModeEnum.LogoNextToFile] = 90,
                _a[OpeningModeEnum.IconNextToFile] = 40,
                _a[OpeningModeEnum.IconNextToFileWithText] = 130,
                _a);
            var width = widthByMode[this.params.plugin.openingMode] || 90;
            var a = $("<button class=\"btn btn-link d-inline-block p-0 mx-4 my-2 amanote-button position-relative\" aria-label=\"".concat(this.params.strings.annotateResource, "\" style=\"z-index: 4\">\n                    <img src=\"").concat(logo, "\" width=\"").concat(width, "px\" aria-hidden=\"true\">\n                  </button>"));
            if (this.params.plugin.openingMode === OpeningModeEnum.LogoNextToFile) {
                a.css('min-width', '110px');
            }
            a.attr(CourseModuleFilter.annotatableIDAttribute, annotatable.id);
            return a;
        };
        CourseModuleFilter.prototype.replaceLink = function (link, annotatable) {
            link.attr(CourseModuleFilter.annotatableIDAttribute, annotatable.id);
            link.addClass('amanote-button');
            link.css('cursor', 'pointer');
        };
        CourseModuleFilter.prototype.addOnDeleteWarning = function (activity) {
            var _this = this;
            activity.find('.editing_delete').first().on('click', function () {
                setTimeout(function () { _this.menuModal.showDeleteWarning(); }, 500);
            });
        };
        CourseModuleFilter.annotatableIDAttribute = 'annotatable-id';
        CourseModuleFilter.amanoteButtonClass = 'amanote-button';
        return CourseModuleFilter;
    }());
    var MenuModal = (function () {
        function MenuModal() {
            this.moodleService = MoodleService.getInstance();
            this.modalFactory = this.moodleService.getModalFactory();
            this.pluginParams = this.moodleService.getPluginParams();
            this.moodleUserParams = this.moodleService.getUserParams();
        }
        MenuModal.prototype.open = function (annotatable) {
            if (!annotatable) {
                return;
            }
            var modalParams = {
                title: 'Amanote',
                body: this.generateModalBodyHTML(annotatable),
                footer: '',
            };
            return this.modalFactory.create(modalParams)
                .then(function (modal) {
                modal.show();
            });
        };
        MenuModal.prototype.showDeleteWarning = function () {
            var hideDeleteWarningExp = localStorage.getItem(StorageKeysEnum.HideDeleteWarningExp);
            if (hideDeleteWarningExp && !isNaN(Date.parse(hideDeleteWarningExp))) {
                var hideDate = new Date(hideDeleteWarningExp);
                var now = new Date();
                if ((now.getTime() - hideDate.getTime()) < (30 * 24 * 60 * 60 * 1000)) {
                    return;
                }
            }
            var guideLink = 'https://help.amanote.com/en/support/solutions/articles/36000448676';
            var message = "<p class=\"alert alert-warning\">\n        ".concat(this.pluginParams.strings.deletefilewarning, "\n        <a href=\"").concat(guideLink, "\" rel=\"noopener\" target=\"_blank\" class=\"d-block mt-3\">").concat(this.pluginParams.strings.seeguide, "</a>\n      </p>\n      <div class=\"text-center\">\n        <button class=\"btn btn-link text-dark\" data-action=\"hide\"\n        onclick=\"localStorage.setItem('").concat(StorageKeysEnum.HideDeleteWarningExp, "', new Date().toISOString());\">\n          ").concat(this.pluginParams.strings.stopmodal, "\n        </button>\n      </div>");
            var modalParams = {
                title: 'Amanote Warning',
                body: message,
                footer: '',
            };
            this.modalFactory.create(modalParams)
                .then(function (modal) {
                modal.show();
            });
        };
        MenuModal.prototype.generateModalBodyHTML = function (annotatable) {
            var openInAmanoteURL = this.moodleService.generateAmanoteURL(annotatable, 'note-taking');
            var body = "<p class=\"mb-0 text-dark\">".concat(this.pluginParams.strings.modalDescription, "</p>");
            body += MenuModal.generateButtonHTML(openInAmanoteURL, this.pluginParams.strings.annotateResource, 'fa fa-edit', 'font-weight: 600; background: #2cdf90; color: #03341f; border: none');
            if (this.pluginParams.plugin.openingMode === OpeningModeEnum.FileClick && !this.pluginParams.plugin.preventDownload) {
                body += MenuModal.generateButtonHTML(annotatable.openInMoodleURL, this.pluginParams.strings.viewResource, 'fa fa-eye', 'font-weight: 600;');
            }
            if (this.moodleUserParams.isTeacher && annotatable.kind === ContentKindEnum.Document) {
                body += '<hr class="mb-0" aria-hidden="true">';
                var openAnalyticsURL = this.moodleService.generateAmanoteURL(annotatable, "document-analytics/".concat(annotatable.id, "/view"));
                body += MenuModal.generateButtonHTML(openAnalyticsURL, this.pluginParams.strings.openAnalytics);
                if (this.pluginParams.plugin.key && !this.pluginParams.plugin.anonymous) {
                    var openPodcastCreatorURL = this.moodleService.generateAmanoteURL(annotatable, 'podcast/creator');
                    body += MenuModal.generateButtonHTML(openPodcastCreatorURL, this.pluginParams.strings.openPodcastCreator);
                }
                if (this.pluginParams.plugin.worksheet && !this.pluginParams.plugin.anonymous) {
                    var openStudentWorkURL = this.moodleService.generateAmanoteURL(annotatable, "document-analytics/".concat(annotatable.id, "/notes"));
                    body += MenuModal.generateButtonHTML(openStudentWorkURL, this.pluginParams.strings.openStudentsWorks);
                }
            }
            if (this.pluginParams.plugin.openingMode === OpeningModeEnum.FileClick &&
                !this.pluginParams.plugin.preventDownload) {
                body += "\n          <div class=\"text-center mt-3\">\n            <button class=\"btn btn-link text-dark\" data-action=\"hide\"\n            onclick=\"localStorage.setItem('".concat(StorageKeysEnum.OpeningMode, "', '").concat(OpeningModeEnum.IconNextToFile, "'); location.reload();\">\n              ").concat(this.pluginParams.strings.stopmodal, "\n            </button>\n          </div>");
            }
            return body;
        };
        MenuModal.generateButtonHTML = function (href, title, faIconClass, style) {
            if (style === void 0) { style = ''; }
            var faIconHTML = '';
            if (faIconClass) {
                faIconHTML = "<i class=\"".concat(faIconClass, " mr-2\" aria-hidden=\"true\"></i> ");
            }
            return "<a class=\"btn btn-secondary w-100 mt-3\" style=\"".concat(style, "\" href=\"").concat(href, "\" target=\"_blank\">\n                ").concat(faIconHTML).concat(title, "\n              </a>");
        };
        return MenuModal;
    }());
    return new Main();
});
var OpeningModeEnum;
(function (OpeningModeEnum) {
    OpeningModeEnum["FileClick"] = "0";
    OpeningModeEnum["LogoNextToFile"] = "1";
    OpeningModeEnum["IconNextToFile"] = "2";
    OpeningModeEnum["IconNextToFileWithText"] = "3";
})(OpeningModeEnum || (OpeningModeEnum = {}));
var OpeningTargetEnum;
(function (OpeningTargetEnum) {
    OpeningTargetEnum["Amanote"] = "0";
    OpeningTargetEnum["MoodleFullscreen"] = "1";
    OpeningTargetEnum["MoodleEmbedded"] = "2";
})(OpeningTargetEnum || (OpeningTargetEnum = {}));
var ContentKindEnum;
(function (ContentKindEnum) {
    ContentKindEnum["Document"] = "document";
    ContentKindEnum["Video"] = "video";
})(ContentKindEnum || (ContentKindEnum = {}));
var StorageKeysEnum;
(function (StorageKeysEnum) {
    StorageKeysEnum["OpeningMode"] = "amanote.preferences.openingMode";
    StorageKeysEnum["HideDeleteWarningExp"] = "amanote.preferences.hideDeleteWarningExp";
})(StorageKeysEnum || (StorageKeysEnum = {}));
//# sourceMappingURL=main.js.map