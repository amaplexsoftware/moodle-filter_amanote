<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote annotation page.
 *
 * @package     filter_amanote
 * @copyright   2021 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('../../lib/externallib.php');
require_once('./amanote.php');

// Verify that the current user is logged in.
require_login();

// Initialize params.
$annotatableid = isset($_GET['annotatableId']) ? $_GET['annotatableId'] : null;

if ($annotatableid == null) {
    // Backward compatibility.
    $annotatableid = isset($_GET['amaResourceId']) ? $_GET['amaResourceId'] : null;
}

$downloadnotes = isset($_GET['downloadNotes']) ? $_GET['downloadNotes'] : null;

if (!validate_annotatableid($annotatableid)) {
    echo 'Invalid resource id.';
    exit();
}

// Set page details.
$explodedid = explode('.', $annotatableid);
$courseid = $explodedid[0];
$moodleurl = new moodle_url('/filter/amanote/annotate.php');

$PAGE->set_context(context_course::instance($courseid));
$PAGE->set_url($moodleurl);
$PAGE->set_title(get_string('filtername', 'filter_amanote'));

// Render the app.
try {
    $config = get_config('filter_amanote');
    $amanoteurl = generate_amanote_url($annotatableid);

    if ($downloadnotes) {
        $amanoteurl = $amanoteurl . '&downloadNotes=true';
    }

    if ($config->target == 1) {
        echo '<style> body { margin: 0 !important; } </style>';
        echo '<iframe width="100%" style="height: 100vh" frameBorder="0" src="' . $amanoteurl . '" allow="autoplay"></iframe>';
    } else {
        echo "<style>
                #region-main, .card-body { padding: 0 !important; }
                #page-footer, #page-wrapper::after { display: none !important; }
              </style>";

        echo $OUTPUT->header();

        echo '<iframe width="100%" style="height: calc(100vh - 58px)" frameBorder="0" ' .
             'src="' . $amanoteurl . '" allow="autoplay"></iframe>';

        echo $OUTPUT->footer();
    }
} catch (Exception $e) {
    debugging('An error occurred: ' . $e->getMessage() . $e->getTraceAsString(), DEBUG_DEVELOPER);
    echo 'An error occured.';
}
