<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote external functions and service definitions.
 *
 * @package     filter_amanote
 * @copyright   2020 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;


$functions = [
    'filter_amanote_can_update_course' => [
        'classname' => 'filter_amanote_external',
        'methodname' => 'can_update_course',
        'classpath' => 'filter/amanote/externallib.php',
        'description' => 'Check whether the current user can update a course.',
        'type' => 'read',
        'ajax' => true,
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
    'filter_amanote_notify' => [
        'classname' => 'filter_amanote_external',
        'methodname' => 'notify',
        'classpath' => 'filter/amanote/externallib.php',
        'description' => 'Send a Moodle notification.',
        'type' => 'read',
        'ajax' => true,
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
    'filter_amanote_get_amanote_url' => [
        'classname' => 'filter_amanote_external',
        'methodname' => 'get_amanote_url',
        'classpath' => 'filter/amanote/externallib.php',
        'description' => 'Get the Amanote app URL for a given annotatable.',
        'type' => 'read',
        'ajax' => true,
        'services' => [MOODLE_OFFICIAL_MOBILE_SERVICE],
    ],
];
