<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote file helper functions.
 *
 * @package     filter_amanote
 * @copyright   2021 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Try to guess the mimetype from an URL.
 *
 * @param string $url The url.
 *
 * @return string The mimetype if one is found, application/unknown otherwise.
 */
function guess_url_mimetype($url) {

    $mimetype = 'application/unknown';

    if (!$url) {
        return $mimetype;
    }

    // Check if the url matches a supported video provider (youtube or vimeo).
    preg_match("/^(https?\:\/\/)?(www\.)?(youtube|youtu|vimeo)\.(com|be)\/.+$/", $url, $matches);
    if ($matches && count($matches) > 3) {
        return strpos($matches[3], 'youtu') !== false ? 'video/youtube' : 'video/vimeo';
    }

    // Check if the url ends with a file format.
    preg_match("/\.(\w{3,4})($|\?|\#)/", $url, $matches);
    if (!$matches || count($matches) < 2) {
        return $mimetype;
    }

    $extension = $matches[1];
    $types = core_filetypes::get_types();

    if (isset($types[$extension])) {
        $mimetype = $types[$extension]['type'];
    }

    return $mimetype;
}

/**
 * Get the content kind from a mimetype.
 *
 * @param string $mimetype The mimetype.
 *
 * @return string The content kind if one is found, unknown otherwise.
 */
function get_content_kind_from_mimetype($mimetype) {

    if ($mimetype === 'video/youtube' || $mimetype === 'video/vimeo') {
        return 'video';
    }

    $types = core_filetypes::get_types();

    foreach ($types as $type) {
        if ($type['type'] === $mimetype && isset($type['groups']) && !empty($type['groups'])) {
            return $type['groups'][0];
        }
    }

    return 'unknown';
}

/**
 * Get the supported mimetypes for a given (optional) content kind. If no
 * content kind is given, all the supported mimtypes are returned.
 *
 * @param string $contentkind The content kind if any.
 *
 * @return string The supported mimetypes.
 */
function get_supported_mimetypes($contentkind = null) {
    $documentmimetypes = [
        'application/pdf',
        'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/epub+zip',
        'application/vnd.oasis.opendocument.text',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    ];

    $videomimetypes = [
        'video/quicktime',
        'video/mp4',
        'video/mpeg',
        'video/ogg',
        'video/youtube',
        'video/vimeo',
    ];

    if ($contentkind === 'video') {
        return $videomimetypes;
    } else if ($contentkind === 'document') {
        return $documentmimetypes;
    }

    return array_merge($documentmimetypes, $videomimetypes);
}

/**
 * Get the user's notes for a given course.
 *
 * @param string $userid The user id.
 * @param string $courseid The course id.
 *
 * @return array An array of filename.
 */
function get_user_notes_for_course($userid, $courseid) {
    global $DB;

    $sql = "SELECT filename FROM {files} WHERE filepath = '/Amanote/'
        AND filearea = 'private'
        AND userid = :userid
        AND filename LIKE :courseid";

    return $DB->get_records_sql($sql, ['userid' => $userid, 'courseid' => $courseid . '.%']);
}

/**
 * Get the annotatable for a given file.
 *
 * @param file $file The file.
 * @param string $courseid The course id.
 * @param string $cmid The course module id.
 * @param string $modinstance The module instance id.
 *
 * @return annotatable The annotatable.
 */
function get_annotatable_for_file($file, $courseid, $cmid, $modinstance) {
    $annotatable = new annotatable();

    // Generate the annotable id for the file.
    if ($cmid) {
        $annotatable->id = $courseid . '.' . $cmid;
    } else {
        $annotatable->id = $courseid . '.' . $modinstance  . '.' . $file->id;
    }

    $annotatable->cmid = $cmid;

    if ($file->component === 'mod_resource' || $file->component === 'mod_folder') {
        $annotatable->legacyid = $courseid . '.' . $modinstance  . '.' . $file->id;
    }

    // Use the legacy id for folder as all files in the folder has the same cmid.
    if ($file->component === 'mod_folder') {
        $annotatable->id = $annotatable->legacyid;
    }

    // Set the content's kind.
    $annotatable->kind = get_content_kind_from_mimetype($file->mimetype);
    if ($annotatable->kind !== 'video') {
        $annotatable->kind = 'document';
    }

    $annotatable->mimetype = $file->mimetype;
    $annotatable->internal = true;
    $filepath = (empty($file->filepath) ? '/' : $file->filepath) . $file->filename;
    $annotatable->url = "/$file->contextid/$file->component/$file->filearea/1$filepath";

    if ($file->filearea === 'intro') {
        $annotatable->url = str_replace('intro/1', 'intro', $annotatable->url);
    }

    return $annotatable;
}

/**
 * Get the annotatable for a given url.
 *
 * @param string $url The url.
 * @param string $courseid The course id.
 * @param string $cmid The course module id.
 *
 * @return annotatable The annotatable.
 */
function get_annotatable_for_url($url, $courseid, $cmid) {

    $annotatable = new annotatable();

    // Generate the annotable id for the url.
    $annotatable->id = $courseid . '.' . $cmid;
    $annotatable->cmid = $cmid;

    $mimetype = guess_url_mimetype($url->externalurl);

    // Set the content's kind.
    $annotatable->kind = get_content_kind_from_mimetype($mimetype);
    if ($annotatable->kind !== 'video') {
        $annotatable->kind = 'document';
    }

    $annotatable->mimetype = $mimetype;
    $annotatable->internal = false;
    $annotatable->url = $url->externalurl;

    return $annotatable;
}
