<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote notification helper functions.
 *
 * @package     filter_amanote
 * @copyright   2023 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once(__DIR__ . '/../amanote.php');

/**
 * Send a notification to a given user.
 *
 * @param string $kind  The kind of the notification (see db/messages.php).
 * @param int    $courseid  The ID of the course that the notification is related to.
 * @param object $userto  The user object representing the recipient of the notification.
 * @param string $subject The subject line of the notification.
 * @param string $message The body of the message, which will be in HTML format.
 * @param string|null $contexturl Optional URL to redirect the user when clicking on the notification.
 *
 * @return int|bool Returns the ID of the sent message if successful, or false on failure.
 */
function send_notification($kind, $courseid, $userto, $subject, $message, $contexturl = null) {
    // Generate the notification.
    $notification = new \core\message\message();
    $notification->notification = 1;
    $notification->component = 'filter_amanote';
    $notification->name = $kind;
    $notification->courseid = $courseid;
    $notification->userfrom = core_user::get_noreply_user();
    $notification->userto = $userto;
    $notification->subject = $subject;
    $notification->fullmessage = $message;
    $notification->fullmessageformat = FORMAT_HTML;
    $notification->fullmessagehtml = $message;
    $notification->contexturl = $contexturl;

    // Send the notification.
    return message_send($notification);
}

/**
 * Send notifications related to a given annotatable.
 *
 * @param string $kind  The type of notification ('submission', 'feedback', 'question').
 * @param string $annotatableid The annotatable id
 *
 * @return array  An array containing the status of sent notifications.
 */
function send_annotatable_notifications($kind, $annotatableid) {
    global $DB, $USER;

    $notifications = [];
    $explodedid = explode('.', $annotatableid);
    $courseid = $explodedid[0];
    $context = context_course::instance($courseid);

    // Determine the message and context URL based on the notification kind.
    $message = '';
    $contexturl = '';

    switch ($kind) {
        case 'submission':
            $message = '[Amanote] ' . get_string('submissionnotification', 'filter_amanote', fullname($USER));
            $contexturl = generate_amanote_url($annotatableid, null, null, 'document-analytics/' . $annotatableid . '/notes');
            break;

        case 'feedback':
            $course = get_course($courseid);
            $message = '[Amanote] ' . get_string('feedbacknotification', 'filter_amanote', $course->fullname);
            $contexturl = generate_amanote_url($annotatableid, null, null, 'document-analytics/' . $annotatableid . '/view');
            break;

        case 'question':
            $course = get_course($courseid);
            $message = '[Amanote] ' . get_string('questionnotification', 'filter_amanote', $course->fullname);
            $contexturl = generate_amanote_url($annotatableid);
            break;

        default:
            break;
    }

    // Send notifications to teachers.
    $teacherrole = $DB->get_record('role', ['shortname' => 'editingteacher']);
    $teachers = get_role_users($teacherrole->id, $context, false, 'u.*');

    foreach ($teachers as $teacher) {
        $notificationid = send_notification($kind, $courseid, $teacher, $message, $message, $contexturl);

        if ($notificationid) {
            $notifications[] = $notificationid;
        }
    }

    return $notifications;
}
