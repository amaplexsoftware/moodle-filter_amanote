<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote my notes page.
 *
 * @package     filter_amanote
 * @copyright   2024 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('../../lib/externallib.php');
require_once('./amanote.php');

// Verify that the current user is logged in.
require_login();

// Render the app.
try {
    $PAGE->set_context(context_user::instance($USER->id));
    $PAGE->set_url(new moodle_url('/filter/amanote/notes.php'));
    $PAGE->set_title(get_string('filtername', 'filter_amanote'));

    // Generate the URL.
    $config = get_config('filter_amanote');

    $redirecturl = '/spaces';

    if ($config->target == 2) {
        $redirecturl .= '?embedded=1';
    }

    $embedded = $config->target != 0;
    $url = generate_amanote_login_url($redirecturl, $embedded);

    // Render the URL.
    if ($config->target == 0) {
        echo "If you are not redirected please <a href=\"$url\">click here</a>
        <script language='JavaScript'>
        window.location.href='$url';
        </script>";
        return;
    }

    if ($config->target == 1) {
        echo '<style> body { margin: 0 !important; } </style>';
        echo '<iframe width="100%" style="height: 100vh" frameBorder="0" src="' . $url . '"></iframe>';
        return;
    }

    echo "<style>
            #region-main, .card-body { padding: 0 !important; }
            #page-footer, #page-wrapper::after { display: none !important; }
        </style>";

    echo $OUTPUT->header();

    echo '<iframe width="100%" style="height: calc(100vh - 58px)" frameBorder="0" src="' . $url . '"></iframe>';

    echo $OUTPUT->footer();

} catch (Exception $e) {
    debugging('An error occurred: ' . $e->getMessage() . $e->getTraceAsString(), DEBUG_DEVELOPER);
    echo 'An error occured.';
}
