<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the Amanote annotatable model.
 *
 * @package     filter_amanote
 * @copyright   2022 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 /**
  * Amanote annotatable model.
  *
  * @copyright   2022 Amaplex Software
  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
  */
class annotatable {
    /**
     * @var string The annotatable's id.
     */
    public $id;

    /**
     * @var string The annotatable's legacy id.
     */
    public $legacyid;

    /**
     * @var string The id of the course module associated with this annotatable.
     */
    public $cmid;

    /**
     * @var string The annotatable's mimetype.
     */
    public $mimetype;

    /**
     * @var bool Dtermine if the content is internal or external to Moodle.
     */
    public $internal;

    /**
     * @var string The url to get the content.
     */
    public $url;

    /**
     * @var string The annotatable's kid.
     */
    public $kind;
}
