<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote observer definitions.
 *
 * @package     filter_amanote
 * @copyright   2024 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/filelib.php');

/**
 * Amanote observer functions.
 *
 * @copyright  2024 Amaplex Software
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class filter_amanote_observer {
    /**
     * Global signout the user on Amanote when the user logs out of Moodle.
     *
     * @param \core\event\user_loggedout $event The event object.
     */
    public static function user_loggedout(\core\event\user_loggedout $event) {
        global $DB, $CFG;

        try {
            // Get the Moodle mobile service.
            $serviceparams = ['shortname' => MOODLE_OFFICIAL_MOBILE_SERVICE, 'enabled' => 1];
            $service = $DB->get_record('external_services', $serviceparams);

            // Get the user's token.
            $context = \context_system::instance();
            $token = $DB->get_record(
                'external_tokens',
                [
                    'userid' => $event->userid,
                    'externalserviceid' => $service->id,
                    'contextid' => $context->id,
                ]
            );

            if (!$token) {
                return;
            }

            // Sign out the user from Amanote.
            $siteid = preg_replace('(^https?://)', '', $CFG->wwwroot, 1);

            $url = 'https://api.amanote.com/oidc/global-signout?' .
                'provider=moodle&' .
                'siteId=' . $siteid . '&' .
                'token=' . $token->token . '&' .
                'userId=' . $event->userid;

            $curl = new \curl();
            $curl->setHeader('Content-Type: application/json');
            $curl->get($url);

        } catch (\Exception $e) {
            debugging('An error occurred: ' . $e->getMessage() . $e->getTraceAsString(), DEBUG_DEVELOPER);
        }
    }
}
