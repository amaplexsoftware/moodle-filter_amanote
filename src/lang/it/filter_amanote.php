<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English strings for Amanote filter.
 *
 * @package     filter_amanote
 * @copyright   2020 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 $string['annotateresource'] = 'Prendi appunti';
 $string['deletefilewarning'] = 'Se cancelli questa risorsa, gli studenti non avranno più accesso ai loro appunti presi su questo materiale. Se vuoi modificarne il contenuto, é preferibile modificare la risorsa e caricarne una nuova invece che cancellarla. In questo modo gli studenti continueranno ad avere accesso ai loro appunti. Per maggiori dettagli su come fare, segui questa guida.';
 $string['downloadnotes'] = 'Scarica il file annotato';
 $string['feedbacknotification'] = 'Hai ricevuto un nuovo feedback sul corso: {$a}';
 $string['modaldescription'] = 'Puoi prendere note chiare e strutturate direttamente sui tuoi materiali del corso cliccando sul pulsante qui sotto.';
 $string['openanalytics'] = 'Visualizza i feedback degli studenti';
 $string['openpodcast'] = 'Aggiungi spiegazioni orali';
 $string['openstudentsworks'] = 'Apri i lavori degli studenti';
 $string['questionnotification'] = 'C\'è una nuova domanda sul corso: {$a} ';
 $string['seeguide'] = 'Scopri come nel nostro manuale';
 $string['stopmodal'] = 'Non visualizzare più questo messaggio';
 $string['submissionnotification'] = '{$a} ti ha inviato un lavoro';
 $string['takenotesicon'] = 'note-icon-it';
 $string['teacher'] = 'Insegnante';
 $string['viewresource'] = 'Vedi qui';
