<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * French strings for Amanote filter.
 *
 * @package     filter_amanote
 * @copyright   2020 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['annotateresource'] = 'Annoter ce support';
$string['anonymous'] = 'Anonyme';
$string['anonymous_help'] = 'Avec cette option, les utilisateurs sont anonymes dans Amanote et aucune authentification n\'est requise. Cependant, les fonctionnalités de collaboration et de podcast ne seront plus disponibles. Enfin, pour activer cette fonctionnalité, vous devez activer la sauvegarde des notes dans les fichiers personnels et ajouter une clé d\'activation.';
$string['deletefilewarning'] = 'Veuillez noter que si vous supprimez cette ressource, les notes qui y sont associées ne seront plus accessibles aux étudiants. Si votre intention est de mettre à jour la ressource, envisagez de l\'éditer au lieu de la supprimer. Les notes des étudiants seront ainsi préservées.';
$string['downloadnotes'] = 'Télécharger le fichier annoté';
$string['feedbacknotification'] = 'Vous avez reçu un nouveau feedback sur le cours : {$a}';
$string['filtername'] = 'Amanote';
$string['importantinformationdescription'] = 'Afin que le module fonctionne correctement, veuillez vérifier que les exigences suivantes sont respectées:

1. Le plugin Amanote est activé (Administration du site > Plugins > Filtres > Gérer les filters)

2. Les services web sont activés (Administration du site > Fonctions avancées)

3. Le service *Moodle mobile web service* est activé (Administration du site > Plugins > Web services > Services externes)

4. Le protocole REST est activé (Administration du site > Plugins > Services web > Gérer les protocoles)

5. La capacité *webservice/rest:use* est autorisée pour les *utilisateurs authentifiés* (Administration du site > Utilisateurs > Permissions > Définition des rôles > Utilisateur authentifié > Gérer les rôles)';
$string['importantinformationheading'] = 'Important installation information';
$string['key'] = 'Clé d\'activation';
$string['key_help'] = 'Cette clé est requise pour les fonctionalités avancées telles que le créateur de podcasts et le mode anonyme.';
$string['messageprovider:feedback'] = 'Soumission d\'un feedback';
$string['messageprovider:question'] = 'Soumission d\'une question';
$string['messageprovider:submission'] = 'Soumission d\'une prise de notes ou d\'un travail par un étudiant';
$string['modaldescription'] = 'Vous pouvez prendre des notes claires et structurées directement sur ce support de cours en cliquant sur le bouton ci-dessous.';
$string['openanalytics'] = 'Voir les statistiques d\'apprentissage';
$string['openingmode'] = 'Mode d\'ouverture';
$string['openingmode_fileclick'] = 'Clic sur la ressource (meilleure expérience utilisateur)';
$string['openingmode_help'] = 'Sélectionnez la méthode pour annoter une ressource avec Amanote.';
$string['openingmode_iconnexttofile'] = 'Petite icône à côté des ressources';
$string['openingmode_iconnexttofilewithtext'] = 'Bouton "Prise de notes" à côté des ressources';
$string['openingmode_logonexttofile'] = 'Logo Amanote à côté des ressources';
$string['openpodcast'] = 'Ajouter des explications orales';
$string['openstudentsworks'] = 'Ouvrir les travaux des étudiants';
$string['pluginadministration'] = 'Administration du module Amanote';
$string['preventdownload'] = 'Empêcher le téléchargement';
$string['preventdownload_help'] = 'Lorsqu\'elle est activée, cette option empêche les utilisateurs de télécharger le support de cours original (par exemple, le fichier PDF de l\'enseignant).';
$string['privacy:metadata'] = 'Pour s\'intégrer avec Amanote, certaines données utilisateur doivent être envoyées à l\'application Amanote (système distant).';
$string['privacy:metadata:access_token'] = 'Le jeton d\'accès est nécessaire pour sauvegarder les notes dans l\'espace privé Moodle de l\'utilisateur.';
$string['privacy:metadata:access_token_expiration'] = 'La date d\'expiration du jeton d\'accès est envoyée pour empêcher un utilisateur d\'utiliser l\'application avec un jeton expiré.';
$string['privacy:metadata:email'] = 'L\'adresse e-mail de l\'utilisateur est envoyée au système distant pour permettre une meilleure expérience utilisateur (partage de notes, notification, etc.).';
$string['privacy:metadata:fullname'] = 'Le nom complet de l\'utilisateur est envoyé au système distant pour permettre une meilleure expérience utilisateur.';
$string['privacy:metadata:subsystem:corefiles'] = 'Les fichiers (PDF, AMA) sont stockés avec le système de fichiers Moodle.';
$string['privacy:metadata:userid'] = 'Le userid est envoyé depuis Moodle pour accélérer le processus d\'authentification.';
$string['questionnotification'] = 'Une nouvelle question a été postée dans le cours: {$a} ';
$string['saveinprivate'] = 'Sauver les notes dans les fichiers personnels';
$string['saveinprivate_help'] = 'Lorsqu\'elle est activée, cette option permet d\'enregistrer les notes dans les fichiers privés de l\'utilisateur sur Moodle plutôt que sur les serveurs d\'Amanote.';
$string['seeguide'] = 'Regarder comment ici';
$string['stopmodal'] = 'Ne plus afficher ce message';
$string['submissionnotification'] = '{$a} a soumis un travail';
$string['takenotesicon'] = 'note-icon-fr';
$string['target'] = 'Target';
$string['target_help'] = 'Sélectionnez l\'endroit où l\'utilisateur sera redirigé pour annoter la ressource.';
$string['target_inamanote'] = 'Site Amanote (meilleure expérience utilisateur)';
$string['target_inmoodle'] = 'Embarqué dans Moodle';
$string['target_inmoodlefullscreen'] = 'Moodle en plein écran';
$string['teacher'] = 'Professeur';
$string['viewresource'] = 'Consulter ici';
$string['worksheet'] = 'Envoyer à l\'enseignant';
$string['worksheet_help'] = 'Cette option permet aux apprenants d\'envoyer leurs notes, devoirs ou travail au professeur. Ce dernier peut alors corriger les notes et les renvoyer à l\'apprenant.';
