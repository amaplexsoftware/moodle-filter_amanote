<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Amanote functions.
 *
 * @package     filter_amanote
 * @copyright   2020 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot . '/config.php');
require_once($CFG->libdir . '/externallib.php');
require_once(__DIR__ . '/helpers/filehelper.php');
require_once(__DIR__ . '/models/annotatable.php');

// Verify that the current user is logged in.
require_login();

/**
 * Validate an annotatable id.
 *
 * @param string $id The annotatable id.
 *
 * @return boolean True if the id is valid.
 */
function validate_annotatableid($id) {
    return $id && preg_match('/\d+\.\d+(\.\d+)?/', $id);
}

/**
 * Generate the amanote URL from an annotatable id.
 *
 * @param string $annotatableid The annotatable id.
 * @param int $pagenumber The page number to open if any.
 * @param file $file The corresponding file if in any.
 * @param string $route The route in the Amanote app.
 * @param boolean $includetoken Whether to include the token in the URL.
 *
 * @return string The generated url.
 */
function generate_amanote_url($annotatableid, $file = null, $pagenumber = null, $route = 'note-taking', $includetoken = true) {
    global $DB, $CFG, $USER;

    if (!validate_annotatableid($annotatableid)) {
        throw new Exception('Invalid annotatable id.');
    }

    $explodedid = explode('.', $annotatableid);
    $notefilename = $annotatableid . '.ama';
    $annotatable = null;

    if ($file) {
        $annotatable = get_annotatable_for_file($file, $explodedid[0], null, $explodedid[1]);
    } else {
        $annotatable = get_annotatable_by_id($annotatableid);
    }

    if ($annotatable === null) {
        $annotatable = new annotatable();
        $annotatable->id = $annotatableid;
    }

    if ($annotatable->legacyid) {
        $savednotes = get_user_notes_for_course($USER->id, $explodedid[0]);
        if (array_key_exists($annotatable->legacyid . '.ama', $savednotes)) {
            $notefilename = $annotatable->legacyid . '.ama';
        }
    }

    // Get the moodle mobile service.
    $serviceparams = ['shortname' => MOODLE_OFFICIAL_MOBILE_SERVICE, 'enabled' => 1];
    $service = $DB->get_record('external_services', $serviceparams);

    if (empty($service)) {
        throw new Exception('Moodle mobile service not found.');
        exit();
    }

    // Generate the URL.
    $config = get_config('filter_amanote');
    $siteurl = $CFG->wwwroot;
    $language = substr($USER->lang, 0, 2);
    $usercontext = context_user::instance($USER->id);
    $privatefilepath = '/' . $usercontext->id . '/user/private/Amanote/';
    $moodleversion = preg_replace('/(\d+\.\d+(\.\d+)?) .*$/', '$1', $CFG->release);
    $token = $includetoken ? external_generate_token_for_current_user($service) : null;
    $sslenabled = !(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' || $_SERVER['SERVER_PORT'] != 443);
    $protocol = $sslenabled ? 'https' : 'http';

    if ($route === 'note-taking' && $annotatable->kind === 'video') {
        $route = '/note-taking/moodle/video/' . $annotatable->id;
    } else {
        $route = '/moodle/' . $route;
    }

    // Build the parameters.
    $params = [
        'siteURL' => $siteurl,
        'userId' => $USER->id,
        'filePath' => $annotatable->internal ? $annotatable->url : rawurlencode($annotatable->url),
        'amaPath' => $privatefilepath . $notefilename,
        'resourceId' => $annotatable->id,
        'legacyResourceId' => $annotatable->legacyid ?: $annotatable->id,
        'saveInProvider' => $config->saveinprivate,
        'providerVersion' => $moodleversion,
        'pluginVersion' => $config->version,
        'key' => $config->key,
        'worksheet' => $config->worksheet,
        'mimeType' => $annotatable->mimetype,
        'anonymous' => $config->anonymous,
    ];

    // Add token parameters.
    if ($includetoken && $token !== null) {
        $params['accessToken'] = $token->token;
        $params['tokenExpDate'] = $token->validuntil;
    }

    // Add page number parameter.
    if ($pagenumber !== null) {
        $params['pageNumber'] = $pagenumber;
    }

    // Add embedded parameter.
    if ($config->target == 2) {
        $params['embedded'] = 1;
    }

    // Generate the URL.
    return $protocol . '://app.amanote.com/' . $language . $route . '?' . http_build_query($params, '', '&');
}

/**
 * Generate the amanote login URL that allows to login from Moodle to Amanote.
 *
 * @param string|null $redirecturl Optional. The URL to redirect the user to after login. Default is null.
 * @param boolean $embedded Optional. Whether to open Amanote in embedded mode. Default is false.
 *
 * @return string The generated URL.
 */
function generate_amanote_login_url($redirecturl = null, $embedded = false) {
    global $DB, $CFG, $USER;

    // Get the moodle mobile service.
    $serviceparams = ['shortname' => MOODLE_OFFICIAL_MOBILE_SERVICE, 'enabled' => 1];
    $service = $DB->get_record('external_services', $serviceparams);

    if (empty($service)) {
        throw new Exception('Moodle mobile service not found.');
        exit();
    }

    // Generate the URL.
    $config = get_config('filter_amanote');
    $siteurl = $CFG->wwwroot;
    $language = substr($USER->lang, 0, 2);
    $usercontext = context_user::instance($USER->id);
    $token = external_generate_token_for_current_user($service);
    $sslenabled = !(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' || $_SERVER['SERVER_PORT'] != 443);
    $protocol = $sslenabled ? 'https' : 'http';
    $route = '/moodle/auth-redirect';

    $url = $protocol . '://app.amanote.com/' . $language  . $route . '?' .
        'siteURL='.          $siteurl . '&' .
        'accessToken=' .     $token->token . '&' .
        'tokenExpDate=' .    $token->validuntil . '&' .
        'userId=' .          $USER->id;

    if (!empty($redirecturl)) {
        $url .= '&redirectURL=' . rawurlencode($redirecturl);
    }

    if ($embedded) {
        $url .= '&embedded=1';
    }

    return $url;
}

/**
 * Return the annotatable for a given id.
 *
 * @param string $annotatableid The annotatable id to get.
 *
 * @return annotatable The annotatable.
 */
function get_annotatable_by_id($annotatableid) {
    global $DB, $USER;

    $explodedid = explode('.', $annotatableid);

    // Get the file from annotatable id.
    if (count($explodedid) >= 3) {
        $fileid = $explodedid[2];
        $file = $DB->get_record('files', ['id' => $fileid]);

        return get_annotatable_for_file($file, $explodedid[0], null, $explodedid[1]);
    }

    // Get the instance from the cmid.
    $sql = "SELECT instance, name FROM {course_modules}
            INNER JOIN {modules} ON {modules}.id = {course_modules}.module
            WHERE {course_modules}.id = :cmid";

    $cmid = $explodedid[1];
    $cm = $DB->get_record_sql($sql, ['cmid' => $cmid]);

    if (empty($cm)) {
        return null;
    }

    if (in_array($cm->name, ['resource', 'folder', 'label'])) {
        // Get annotatable for file.
        $sql = "SELECT
            {files}.id as id,
            {files}.contextid,
            {files}.mimetype,
            {files}.component,
            {files}.filearea,
            {files}.filename
        FROM {course_modules}
            LEFT JOIN {context} ON {context}.instanceid = {course_modules}.id
            LEFT JOIN {files} ON {files}.contextid = {context}.id
            AND {course_modules}.id = :cmid
            WHERE {files}.component in ('mod_resource', 'mod_label', 'mod_folder')
            AND {files}.source IS NOT NULL AND {files}.filename != '.'";

        $files = $DB->get_records_sql($sql, ['cmid' => $cmid]);

        if (!$files || count($files) <= 0) {
            throw new Exception('File not found.');
        }

        $file = reset($files);

        return get_annotatable_for_file($file, $explodedid[0], $cmid, $cm->instance);
    } else if ($cm->name === 'url') {
        // Get annotatable for url.
        $url = $DB->get_record($cm->name, ['id' => $cm->instance]);

        if (empty($url)) {
            throw new Exception('Course module instance not found.');
        }

        return get_annotatable_for_url($url, $explodedid[0], $cmid);
    } else {
        throw new Exception('Module not supported.');
    }

    return null;
}
