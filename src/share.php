<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Open a given resource in Amanote.
 *
 * If the user doesn't have access to the resource, it will search if there is another
 * corresponding resource in an other course (from backup/restore, for instance) in
 * which the current user is enrolled in.
 *
 * @package     filter_amanote
 * @copyright   2021 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../config.php');
require_once('../../lib/externallib.php');
require_once('./amanote.php');

// Verify that the current user is logged in.
require_login();

// Initialize params.
$annotatableid = isset($_GET['annotatableId']) ? $_GET['annotatableId'] : null;

if ($annotatableid == null) {
    // Backward compatibility.
    $annotatableid = isset($_GET['amaResourceId']) ? $_GET['amaResourceId'] : null;
}

$pagenumber = isset($_GET['pageNumber']) ? $_GET['pageNumber'] : null;
$file = null;

if (!validate_annotatableid($annotatableid)) {
    echo 'Invalid resource id.';
    exit();
}

$explodedid = explode('.', $annotatableid);

// Get the file corresponding to the given id in a course in which the user is enrolled.
if (count($explodedid) >= 3) {
    $courseid = $explodedid[0];
    $modid = $explodedid[1];
    $fileid = $explodedid[2];

    $sql = "SELECT
            {files}.id AS id,
            {files}.contextid,
            {files}.component,
            {files}.filearea,
            {files}.filename,
            {files}.filepath,
            {files}.mimetype,
            {course_modules}.instance AS modid,
            {course_modules}.course AS courseid
            FROM {files}
            INNER JOIN {context} ON {context}.id = {files}.contextid
            INNER JOIN {course_modules} ON {course_modules}.id = {context}.instanceid
            WHERE (contenthash, timecreated)=
                (SELECT contenthash,timecreated FROM {files}
                WHERE {files}.id=:fileid)
            AND {course_modules}.course IN
                (SELECT courseId FROM {enrol}
                INNER JOIN {user_enrolments} ON {enrol}.id = {user_enrolments}.enrolid
                WHERE {user_enrolments}.userid=:userid)
            ORDER BY ABS({files}.id - :fileid2) ASC";

    $result = $DB->get_records_sql($sql, ['fileid' => $fileid, 'userid' => $USER->id, 'fileid2' => $fileid]);

    if (!$result || count($result) <= 0) {
        echo 'You don\'t have access to this resource.';
        exit();
    }

    $files = array_values($result);
    $file = array_shift($files);

    $annotatableid = $file->courseid . '.' . $file->modid . '.' . $file->id;
}

// Generate the Amanote's URL.
$url = generate_amanote_url($annotatableid, $file, $pagenumber);

// Redirect to Amanote.
echo "If you are not redirected please <a href=\"$url\">click here</a>
<script language='JavaScript'>
window.location.href='$url';
</script>";
