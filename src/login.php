<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Login from Moodle to Amanote.
 *
 * @package     filter_amanote
 * @copyright   2022 Amaplex Software
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('../../lib/externallib.php');
require_once('./amanote.php');

// Verify that the current user is logged in.
require_login();

// Generate the login URL.
$url = generate_amanote_login_url();

// Redirect to Amanote.
echo "If you are not redirected please <a href=\"$url\">click here</a>
<script language='JavaScript'>
window.location.href='$url';
</script>";
